﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;

namespace CoolParking.UI.Models
{
    class Vehicle
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("vehicleType")]
        public int VehicleType { get; set; }

        [JsonProperty("balance")]
        public decimal Balance { get; set; }

        public override string ToString()
        {
            return $"ID: {Id}\nType: {VehicleType}\nBalance: {Balance}";
        }

       
    }


}