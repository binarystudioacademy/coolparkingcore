﻿using Newtonsoft.Json;

namespace CoolParking.UI.Models
{
    public class Payment
    {
        [JsonProperty]
        public string Id { get; set; }
        [JsonProperty]
        public decimal Sum { get; set; }
    }
}
