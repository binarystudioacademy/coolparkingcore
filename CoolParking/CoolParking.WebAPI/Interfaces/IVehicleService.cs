﻿using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;
using System.Collections.Generic;

namespace CoolParking.WebAPI.Interfaces
{
    public interface IVehicleService
    {
        List<Vehicle> GetVehicles();
        Vehicle GetById(string id);
        Vehicle Create(VehicleRequestData vehicle);
        void DeleteVehicle(string id);
    }
}
