﻿using CoolParking.BL.Models;

namespace CoolParking.WebAPI.Interfaces
{
    public interface ITransactionService
    {
        TransactionInfo[] GetLastTransactions();
        string GetAllTransactions();
        Vehicle TopUpVehicle(string id, decimal sum);
    }
}
