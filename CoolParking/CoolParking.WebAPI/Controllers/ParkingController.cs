﻿using CoolParking.WebAPI.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public ParkingController(IParkingService parkingSessionService)
        {
            _parkingService = parkingSessionService;
        }

        // GET api/parking/balance
        [HttpGet("[action]")]
        public ActionResult<decimal> Balance()
        {
            return _parkingService.GetBalance();
        }

        // GET api/parking/capacity
        [HttpGet("[action]")]
        public ActionResult<int> Capacity()
        {
            return _parkingService.GetCapacity();
        }

        // GET api/parking/freePlaces
        [HttpGet("[action]")]
        public ActionResult<int> FreePlaces()
        {
            return _parkingService.GetFreePlaces();
        }
    }
}