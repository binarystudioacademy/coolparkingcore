﻿using CoolParking.BL.Models;
using CoolParking.WebAPI.Interfaces;
using CoolParking.WebAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using System;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly ITransactionService _transactionService;

        public TransactionsController(ITransactionService transactionService)
        {
            _transactionService = transactionService;
        }

        // GET api/transactions/last
        [HttpGet("[action]")]
        public ActionResult<TransactionInfo[]> Last()
        {
            return _transactionService.GetLastTransactions();
        }

        // GET api/transactions/all
        [HttpGet("[action]")]
        public ActionResult<string> All()
        {
            try
            {
                return _transactionService.GetAllTransactions();
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
        }

        // PUT api/transactions/topUpVehicle
        [HttpPut("[action]")]
        public ActionResult<Vehicle> TopUpVehicle(Payment payment)
        {
            if (!Vehicle.IsValidRegistrationPlateNumber(payment.Id) || (decimal)payment.Sum <= 0.0M)
            {
                return BadRequest();
            }

            try
            {
                return _transactionService.TopUpVehicle(payment.Id, (decimal)payment.Sum);
            }
            catch (ArgumentException)
            {
                return NotFound();
            }
        }
    }
}