﻿using CoolParking.BL.Models;
using CoolParking.WebAPI.Interfaces;
using CoolParking.WebAPI.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {        
        private readonly IVehicleService _vehicleService;

        public VehiclesController(IVehicleService vehicleService)
        {           
            _vehicleService = vehicleService;
        }

        // GET api/vehicles
        [HttpGet]
        public ActionResult<List<Vehicle>> GetVehicles()
        {
            return _vehicleService.GetVehicles();
        }

        // GET api/vehicles/id
        [HttpGet("{id}")]
        public ActionResult<Vehicle> GetById(string id)
        {
            if (!Vehicle.IsValidRegistrationPlateNumber(id))
            {
                return BadRequest();
            }

            try
            {
                return _vehicleService.GetById(id);
            }
            catch (ArgumentNullException)
            {
                return NotFound();
            }
        }

        // POST api/vehicles
        [HttpPost]
        public ActionResult<Vehicle>Create(VehicleRequestData vehicle)
        {
            try
            {
                var response = _vehicleService.Create(vehicle);
                return CreatedAtAction(nameof(GetById), new { id = vehicle.Id }, response);
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }

        // DELETE api/vehicles/id
        [HttpDelete("{id}")]
        public IActionResult DeleteVehicle(string id)
        {
            if (!Vehicle.IsValidRegistrationPlateNumber(id))
            {
                return BadRequest();
            }

            try
            {
                _vehicleService.DeleteVehicle(id);
                return NoContent();
            }
            catch (ArgumentException)
            {
                return NotFound();
            }
            catch (InvalidOperationException)
            {
                return Conflict();
            }
        }
    }
}