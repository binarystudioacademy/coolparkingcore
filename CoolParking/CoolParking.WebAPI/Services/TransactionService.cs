﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly BL.Interfaces.IParkingService _parkingService;
        private readonly IVehicleService _vehicleService;

        public TransactionService(BL.Interfaces.IParkingService parkingService, IVehicleService vehicleService)
        {
            _vehicleService = vehicleService;
            _parkingService = parkingService;
        }

        string ITransactionService.GetAllTransactions()
        {
            return _parkingService. ReadFromLog();
        }

        TransactionInfo[] ITransactionService.GetLastTransactions()
        {
            return _parkingService.GetLastParkingTransactions();
        }

        Vehicle ITransactionService.TopUpVehicle(string id, decimal sum)
        {
            _parkingService.TopUpVehicle(id, sum);
            return  _vehicleService.GetById(id);
        }
    }
}
