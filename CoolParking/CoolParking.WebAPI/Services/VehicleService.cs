﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Interfaces;
using CoolParking.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CoolParking.WebAPI.Services
{
    public class VehicleService : IVehicleService
    {
        private BL.Interfaces.IParkingService _parkingService;

        public VehicleService(BL.Interfaces.IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        public List<Vehicle> GetVehicles()
        {
            return new List<Vehicle>(_parkingService.GetVehicles());
        }

        public Vehicle Create(VehicleRequestData vehicleData)
        {
            var vehicle = new Vehicle(vehicleData.Id, (VehicleType)vehicleData.VehicleType, (decimal)vehicleData.Balance);
            _parkingService.AddVehicle(vehicle);
            return vehicle;
        }

        public Vehicle GetById(string id)
        {
            var vehicle = _parkingService.GetVehicles().FirstOrDefault(x => x.Id == id) ??
               throw new ArgumentNullException("Vehicle with the specified ID is not parked");

            return vehicle;
        }

        public void DeleteVehicle(string id)
        {
            _parkingService.RemoveVehicle(id);
        }
    }
}
