﻿using CoolParking.WebAPI.Interfaces;

namespace CoolParking.WebAPI.Services
{
    public class ParkingService : IParkingService
    {
        private readonly BL.Interfaces.IParkingService _parkingService;

        public ParkingService(BL.Interfaces.IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        public decimal GetBalance()
        {
            return _parkingService.GetBalance();
        }

        public int GetCapacity()
        {
            return _parkingService.GetCapacity();
        }

        public int GetFreePlaces()
        {
            return _parkingService.GetFreePlaces();
        }
    }
}