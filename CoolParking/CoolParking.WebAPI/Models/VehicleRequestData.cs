﻿using Newtonsoft.Json;
using System.Text.Json;

namespace CoolParking.WebAPI.Models
{
    public class VehicleRequestData
    {
        int vehicleType = -1;
        decimal balance = 0.0M;
        public string Id { get; set; }
        public object VehicleType
        {
            get
            {
                return vehicleType;
            }
            set
            {
                if (((JsonElement)value).ValueKind == JsonValueKind.String)
                {
                    vehicleType = -1;
                }
                else
                {
                    vehicleType = JsonConvert.DeserializeObject<int>(value.ToString());
                }
            }
        }
        public object Balance
        {
            get
            {
                return balance;
            }
            set
            {
                if (((JsonElement)value).ValueKind == JsonValueKind.String)
                {
                    balance = 0.0M;
                }
                else
                {
                    balance = JsonConvert.DeserializeObject<decimal>(value.ToString());
                }
            }
        }
    }
}
