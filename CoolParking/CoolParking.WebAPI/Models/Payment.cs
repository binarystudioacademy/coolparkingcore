﻿using Newtonsoft.Json;
using System.Text.Json;

namespace CoolParking.WebAPI.Models
{
    public class Payment
    {
        decimal sum = 0.0M;
        public string Id { get; set; }
        public object Sum
        { 
            get
            {
                return sum;
            } 
            set
            {
                if (((JsonElement)value).ValueKind == JsonValueKind.String)
                {
                    sum = 0.0M;
                }
                else
                {
                    sum = JsonConvert.DeserializeObject<decimal>(value.ToString());
                }
            }
        }
    }
}
